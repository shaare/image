# shaare/image  

[**Image Server (Open-Source)**](https://gitea.com/shaare/image) | [Website Backend (Private)](https://gitea.com/shaare/backend) | [Website Frontend (Private)](https://gitea.com/shaare/frontend)


## License & Information
This project is licensed under the [MIT License](https://gitea.com/shaare/image/src/branch/master/LICENSE) and is based on [Moquo/node-sharex-server on GitHub](https://github.com/Moquo/node-sharex-server) which is also licensed under the [MIT License](https://github.com/Moquo/node-sharex-server/blob/master/LICENSE)