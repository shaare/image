require("dotenv").config({ path: "../" });
const config = require("../config.json")
const version = require("../package.json").version;
const logg = require("logg.js")
const randomString = require("random-string");
const path = require("path");
const fs = require("fs");
const fileExists = require("file-exists");
const calipers = require("calipers")("png", "jpeg", "gif", "webp");
const Canvas = require("canvas");

const mysql = require("mysql2");
let connection = new mysql.createConnection(config.mysql)

const { WebhookClient, RichEmbed } = require("discord.js");

// Create /uploads directory if not exists
if(!fs.existsSync("./uploads/")) {
    fs.mkdirSync("./uploads/");
    logg.info("Created /uploads directory", "GENERAL");
}

var express = require("express");
var app = express();
// Static directory for files
app.use("/", express.static("./uploads"));

// body-parser middleware
var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// express-fileupload middleware
var fileUpload = require("express-fileupload");
app.use(fileUpload({
    safeFileNames: true,
    preserveExtension: true,
    limits: {
        fileSize: config.fileSizeLimit
    }
}));

// Index
app.get("/", function(req, res) {
    res.redirect("https://shaare.ga");
});

// Upload file
app.post("/upload", function(req, res) {
    // Check if key is set
    if(!req.body.key) {
        res.status(400).json({
            success: false,
            error: {
                message: "Key is empty.",
                fix: "Submit a key."
            }
        });
    } else {
        // Check if key is registered
        var key = req.body.key;
        connection.query("SELECT * FROM user_accounts WHERE token = ? LIMIT 1;", [key], (error, result) => {
            if(result.length != 1) {
                logg.info("Failed authentication with key " + key, "AUTH");
                res.status(401).json({
                    success: false,
                    error: {
                        message: "Key is invalid.",
                        fix: "Submit a valid key."
                    }
                });
            } else {
                // Key is valid
                logg.info("Authentication with from user " + result[0]["username"] + "(" + result[0]["uId"] + ") succeeded", "AUTH");
                // Check if file was uploaded
                if(!req.files.file) {
                    logg.info("No file was sent, aborting... (UID:" + result[0]["uId"] + ")", "UPLOAD");
                    res.status(400).json({
                        success: false,
                        error: {
                            message: "No file was uploaded.",
                            fix: "Upload a file."
                        }
                    });
                } else {
                    // File was uploaded
                    var file = req.files.file;
                    // Generate the path
                    var fileExtension = path.extname(file.name);
                    var newFileName = randomString({length: config.fileNameLength}) + fileExtension;
                    if(!fs.existsSync("./uploads/" + result[0]["uId"] + "/")) {
                        fs.mkdirSync("./uploads/" + result[0]["uId"] + "/");
                        logg.info("Created /uploads/" + result[0]["uId"] + " directory", "GENERAL");
                    }
                    var uploadPath = __dirname + "/uploads/" + result[0]["uId"] + "/" + newFileName;
    
                    // Check file extension (if enabled)
                    if(config.fileExtensionCheck.enabled && config.fileExtensionCheck.extensionsAllowed.indexOf(fileExtension) == -1) {
                        // Invalid file extension
                        logg.info("File " + file.name + " has an invalid extension, aborting... (UID:" + result[0]["uId"] + ")", "UPLOAD");
                        res.status(400).json({
                            success: false,
                            error: {
                                message: "Invalid file extension.",
                                fix: "Upload a file with a valid extension."
                            }
                        });
                    } else {
                        let uploadId = randomString({length: 5});
                        let webhookUrl = config.webhooks.images;
                        let webhook = new WebhookClient(webhookUrl.split("/")[5], webhookUrl.split("/")[6]);
                        logg.info("Uploading file... (UID:" + result[0]["uId"] + ")", "UPLOAD");
                        webhook.send(new RichEmbed().setTitle("Upload start").setDescription("User " + result[0]["username"] + " (" + result[0]["uId"] + ") started upload of file...").setColor("#FFC400").setFooter("Task #" + uploadId).setTimestamp(new Date()));
    
                        // Move files
                        file.mv(uploadPath, function(err) {
                            if(err) {
                                logg.error(err + " (UID:" + result[0]["uId"] + ")", "UPLOAD");
                                webhook.send(new RichEmbed().setTitle("Upload failed").setDescription("Upload failed: \n```" + err + "```").setColor("#FF0F0F").setFooter("Task #" + uploadId).setTimestamp(new Date()));        
                                webhook.destroy();
                                return res.status(500).send(err);
                            }
    
                            // Return the informations
                            logg.info("Uploaded file... (UID:" + result[0]["uId"] + ")", "UPLOAD");
                            webhook.send(new RichEmbed().setTitle("Upload successful").setDescription("Upload successful").setColor("#4AFF4A").setFooter("Task #" + uploadId).setTimestamp(new Date()));
                            webhook.destroy();
                            res.json({
                                success: true,
                                file: {
                                    url: config.serverUrl + "/" + result[0]["uId"] + "/" + newFileName,
                                    delete_url: config.serverUrl + "/delete?filename=" + newFileName + "&key=" + key
                                }
                            });
                        });
                    }
                }
            }
        })
    }
});

// Delete file
app.get("/delete", function(req, res) {
    if(!req.query.filename || !req.query.key) {
        res.status(400).json({
            success: false,
            error: {
                message: "Key and/or file name is empty.",
                fix: "Submit a key and/or file name."
            }
        });
    } else {
        // Check if key is registered
        var key = req.query.key;
        connection.query("SELECT * FROM user_accounts WHERE token = ? LIMIT 1;", [key], (error, result) => {
            if(result.length != 1) {
                logg.info("Failed authentication with key " + key, "AUTH");
                res.status(401).json({
                    success: false,
                    error: {
                        message: "Key is invalid.",
                        fix: "Submit a valid key."
                    }
                });
            } else {
                // Key is valid
                logg.info("Authentication with from user " + result[0]["username"] + "(" + result[0]["uId"] + ") succeeded", "AUTH");
                // Generate file informations
                var fileName = req.query.filename;
                var filePath = __dirname + "/uploads/" + result[0]["uId"] + "/" + fileName;
                logg.info("Trying to delete file... " + fileName + " (UID:" + result[0]["uId"] + ")", "DELETION");
                let deleteId = randomString({length: 5});
                let webhookUrl = config.webhooks.images;
                let webhook = new WebhookClient(webhookUrl.split("/")[5], webhookUrl.split("/")[6]);
                webhook.send(new RichEmbed().setTitle("Deletion start").setDescription("User " + result[0]["username"] + " (" + result[0]["uId"] + ") started deletion of file...").setColor("#FFC400").setFooter("Task #" + deleteId).setTimestamp(new Date()));
    
                // Check if file exists
                fileExists(filePath, function(err, exists) {
                    if(err) {
                        logg.error(err + " (UID:" + result[0]["uId"] + ")", "DELETION");
                        webhook.send(new RichEmbed().setTitle("Deletion failed").setDescription("Deletion failed: \n```" + err + "```").setColor("#FF0F0F").setFooter("Task #" + deleteId).setTimestamp(new Date()));        
                        webhook.destroy();
                        return res.status(500).send(err);
                    }
    
                    if(!exists) {
                        // File doesnt exists
                        logg.info("File " + fileName + " doesn't exist, aborting... (UID:" + result[0]["uId"] + ")", "DELETION");
                        res.status(400).json({
                            success: false,
                            error: {
                                message: "The file doesnt exists.",
                                fix: "Submit a existing file name."
                            }
                        });
                    } else {
                        // File exists => Delete file
                        fs.unlink(filePath, function(err) {
                            if(err) {
                                logg.error(err + " (UID:" + result[0]["uId"] + ")", "DELETION");
                                webhook.send(new RichEmbed().setTitle("Deletion failed").setDescription("Deletion failed: \n```" + err + "```").setColor("#FF0F0F").setFooter("Task #" + deleteId).setTimestamp(new Date()));
                                webhook.destroy();
                                return res.status(500).send(err);
                            }
    
                            // Return the informations
                            webhook.send(new RichEmbed().setTitle("Deletion successful").setDescription("Deletion successful").setColor("#4AFF4A").setFooter("Task #" + deleteId).setTimestamp(new Date()));
                            webhook.destroy();
                            logg.info("Deleted file " + fileName + " (UID:" + result[0]["uId"] + ")", "DELETION");
                            res.json({
                                success: true,
                                message: "Deleted file " + fileName
                            });
                        });
                    }
                });
            }
        });
    }
});

// Burn file
app.get("/burn", function(req, res) {
    if(!req.query.filename || !req.query.key || !req.query.user || !req.query.reason) {
        res.status(400).json({
            success: false,
            error: {
                message: "Key and/or file name is empty.",
                fix: "Submit a key and/or file name."
            }
        });
    } else {
        // Check if key is registered
        var key = req.query.key;
        connection.query("SELECT * FROM user_accounts WHERE token = ? LIMIT 1;", [key], (error, result) => {
            if(result.length != 1 || (result.length == 1 && result[0]["role"] < 3)) {
                logg.info("Failed authentication with key " + key, "AUTH");
                res.status(401).json({
                    success: false,
                    error: {
                        message: "Key is invalid or not enough permission.",
                        fix: "Submit a valid key."
                    }
                });
            } else {
                // Key is valid
                logg.info("Authentication with from user " + result[0]["username"] + "(" + result[0]["uId"] + ") succeeded", "AUTH");
                // Generate file informations
                var fileName = req.query.filename;
                var filePath = __dirname + "/uploads/" + req.query.user + "/" + fileName;
                logg.info("Trying to burn file... " + fileName + " (UID:" + result[0]["uId"] + ")", "BURNING");
                let deleteId = randomString({length: 5});
                let webhookUrl = config.webhooks.images;
                let webhook = new WebhookClient(webhookUrl.split("/")[5], webhookUrl.split("/")[6]);
                webhook.send(new RichEmbed().setTitle("Burning start").setDescription("User " + result[0]["username"] + " (" + result[0]["uId"] + ") started burning of file " + req.query.user + "/" + fileName + "...").setColor("#FFC400").setFooter("Task #" + deleteId).setTimestamp(new Date()));
    
                // Check if file exists
                fileExists(filePath, function(err, exists) {
                    if(err) {
                        logg.error(err + " (UID:" + result[0]["uId"] + ")", "BURNING");
                        webhook.send(new RichEmbed().setTitle("Burning failed").setDescription("Burning failed: \n```" + err + "```").setColor("#FF0F0F").setFooter("Task #" + deleteId).setTimestamp(new Date()));        
                        webhook.destroy();
                        return res.status(500).send(err);
                    }
    
                    if(!exists) {
                        // File doesnt exists
                        logg.info("File " + fileName + " doesn't exist, aborting... (UID:" + result[0]["uId"] + ")", "BURNING");
                        res.status(400).json({
                            success: false,
                            error: {
                                message: "The file doesnt exists.",
                                fix: "Submit a existing file name."
                            }
                        });
                    } else {
                        // File exists => Delete file
                        calipers.measure(filePath, function(err, metadata) {
                            if(err) {
                                logg.error(err + " (UID:" + result[0]["uId"] + ")", "BURNING");
                                webhook.send(new RichEmbed().setTitle("Burning failed").setDescription("Burning failed: \n```" + err + "```").setColor("#FF0F0F").setFooter("Task #" + deleteId).setTimestamp(new Date()));        
                                webhook.destroy();
                                return res.status(500).send(err);
                            }
                            console.log(metadata.pages[0].width + "x" + metadata.pages[0].height);
                            let canvas = Canvas.createCanvas(metadata.pages[0].width, metadata.pages[0].height);
                            let ctx = canvas.getContext("2d");
                            ctx.fillStyle = "black";
                            ctx.fillRect(0, 0, canvas.width, canvas.height);
                            ctx.textAlign = "center";
                            ctx.textBaseline = "middle";
                            ctx.font = "normal normal bold 20px sans-serif";
                            ctx.fillStyle = "red";
                            ctx.fillText("This file has been burned by the Shaare Team", canvas.width / 2, (canvas.height / 2) - 15);
                            ctx.fillText("Reason: " + req.query.reason, canvas.width / 2, (canvas.height / 2) + 15);
                            let image = canvas.toDataURL();
                            let data = image.replace(/^data:image\/\w+;base64,/, "");
                            let buf = Buffer.from(data, "base64");
                            fs.writeFile(filePath, buf, () => {})
                            webhook.send(new RichEmbed().setTitle("Burning successful").setDescription("Burning successful").setColor("#4AFF4A").setFooter("Task #" + deleteId).setTimestamp(new Date()));
                            webhook.destroy();
                            let webhookUrlBurn = config.webhooks.burn;
                            let webhookBurn = new WebhookClient(webhookUrlBurn.split("/")[5], webhookUrl.split("/")[6]);
                            webhookBurn.send(new RichEmbed().setTitle("Burning").addField("Staff", result[0]["uId"] + " (" + result[0]["username"] + ")", true).addField("User", req.query.user, true).addField("Reason", req.query.reason).setURL(config.serverUrl + "/" + req.params.user + "/" + req.params.filename).setColor("#FFC400").setFooter("Task #" + deleteId).setTimestamp(new Date()));            
                            webhookBurn.destroy();
                            logg.info("Burned file " + fileName + " (UID:" + req.query.user + ")", "BURNING");
                            res.json({
                                success: true,
                                message: "Burned file " + fileName
                            });
                        });
                    }
                });
            }
        });
    }
});

// Start web server
app.listen(config.port, function() {
    logg.info("Now listening on port " + config.port, "WEBSERVER");
});
